import './App.css';
import React, { Fragment, useState } from "react";
import TextField from '@mui/material/TextField';
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider} from "@material-ui/pickers";
import { DatePicker } from "@material-ui/pickers";
import Button from '@mui/material/Button';

function App() {
  const [selectedDate, handleDateChange] = useState(new Date());
  const [firstName1, firstName1Change] = useState("");
  const [lastName1, lastName1Change] = useState("");
  const [firstName2, firstName2Change] = useState("");
  const [lastName2, lastName2Change] = useState("");


  return (
    <div className="App">
     <div className="Form">
     <TextField 
     id="outlined-basic" 
     label="Your first name" 
     variant="outlined"
     onChange={firstName1Change}
    //  style={{ height: 40}}
      />
     <TextField id="outlined-basic" label="Your last name" variant="outlined" onChange={lastName1Change} />
     <TextField id="outlined-basic" label="Their first name" variant="outlined" onChange={firstName2Change} />
     <TextField id="outlined-basic" label="Their last name" variant="outlined" onChange={lastName2Change}/>
     <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <DatePicker
          label="Anniversary Date"
          value={selectedDate}
          onChange={handleDateChange}
          animateYearScrolling
        />
        </MuiPickersUtilsProvider>
        <Button variant="contained">Add to Blockchain</Button>
     </div>
    </div>
  );
}

export default App;
